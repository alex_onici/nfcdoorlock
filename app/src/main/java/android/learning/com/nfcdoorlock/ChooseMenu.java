package android.learning.com.nfcdoorlock;

import android.content.Intent;
import android.learning.com.nfcdoorlock.nfc_normal_mode.ReaderTag;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class ChooseMenu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_menu);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.choose_menu,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()) {
            case R.id.normal_mode:
                Intent intent  = new Intent(this, ReaderTag.class);
                startActivity(intent);
                return true;
            case R.id.service_mode:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
