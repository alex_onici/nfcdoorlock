package android.learning.com.nfcdoorlock.nfc_normal_mode;

import android.content.Intent;
import android.content.SharedPreferences;
import android.learning.com.nfcdoorlock.ChooseMenu;
import android.learning.com.nfcdoorlock.R;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;

public class LoginActivity extends AppCompatActivity {

    private EditText username;
    private EditText password;
    private Button login;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        login = findViewById(R.id.login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    new HttpPostRequest().execute(username.getText().toString(), password.getText().toString());
            }
        });
    }

    private void login(String username, String password) {

    }

    public class HttpPostRequest extends AsyncTask<String, Void, String> {
        public static final String REQUEST_METHOD = "POST";
        public static final int READ_TIMEOUT = 15000;
        public static final int CONNECTION_TIMEOUT = 60000;

        @Override
        protected String doInBackground(String... params) {
            String stringUrl = "http://192.168.43.33:5000/login";
            String result = " ";
            try {
                JSONObject object = createJsonObject(params[0], params[1]);
                //  result = object.toString();
                URL myUrl = new URL(stringUrl);
                HttpURLConnection connection = (HttpURLConnection) myUrl.openConnection(Proxy.NO_PROXY);
                connection.setRequestMethod(REQUEST_METHOD);
                connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                connection.setRequestProperty("Accept", "application/json");
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setConnectTimeout(CONNECTION_TIMEOUT);
                DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
                wr.writeBytes(object.toString());
                wr.flush();
                wr.close();
                connection.getOutputStream().close();
                int responseCode = connection.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    InputStream in = connection.getInputStream();
                    String resultObject = convertStreamToString(in);
                    JSONObject jsonObject = new JSONObject(resultObject);
                    result = jsonObject.getString("id");
                    in.close();
                } else {
                }
            } catch (IOException e) {
                e.printStackTrace();
                result = null;
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return result;
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (!TextUtils.isEmpty(result)) {
                saveUserId(result);
                Intent startIntent = new Intent(LoginActivity.this, ChooseMenu.class);
                startActivity(startIntent);
                finish();
            }
        }
    }

    private void saveUserId(String userId) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        preferences.edit().putString("user_id", userId).commit();
    }

    public static String convertStreamToString(InputStream is) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }


    private JSONObject createJsonObject(String username, String password) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.accumulate("username", username);
        jsonObject.accumulate("password", password);

        return jsonObject;
    }

}
