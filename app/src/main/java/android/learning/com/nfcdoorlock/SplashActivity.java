package android.learning.com.nfcdoorlock;

import android.content.Intent;
import android.learning.com.nfcdoorlock.nfc_normal_mode.LoginActivity;
import android.learning.com.nfcdoorlock.utils.UserUtils;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent startIntent = new Intent(this, ChooseMenu.class);
        if (UserUtils.isUserLoggedIn(this)) {
            startActivity(startIntent);
            finish();
        } else {
            startIntent = new Intent(this, LoginActivity.class);
            startActivity(startIntent);
            finish();
        }
    }
}
