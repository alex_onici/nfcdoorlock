package android.learning.com.nfcdoorlock.nfc_normal_mode;

import android.app.Activity;
import android.content.SharedPreferences;
import android.learning.com.nfcdoorlock.R;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;


public class ClientRegister extends Activity {
    public static final String USER = "User";
    public static final String URL = "http://192.168.43.33:5000/apartamentOwner";



    private Button button;
    private TextView idCard;
    private EditText camera;
    private static int responseCode = 0;

    @Override
    public void onCreate(Bundle savedInstaceState) {
        super.onCreate(savedInstaceState);
        setContentView(R.layout.register_client);
        initUi();
        idCard.setText(getIntent().getStringExtra(USER));
        button = findViewById(R.id.add_client);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //http://192.168.1.101:5000/user
                new HttpPostRequest().execute(URL);
            }
        });
    }


    public class HttpPostRequest extends AsyncTask<String, Void, String> {
        public static final String REQUEST_METHOD = "POST";
        public static final int READ_TIMEOUT = 15000;
        public static final int CONNECTION_TIMEOUT = 60000;

        @Override
        protected String doInBackground(String... params) {
            String stringUrl = params[0];
            String result = " ";
            try {
                JSONObject object = createJsonObject();
                //  result = object.toString();
                URL myUrl = new URL(stringUrl);
                HttpURLConnection connection = (HttpURLConnection) myUrl.openConnection(Proxy.NO_PROXY);
                connection.setRequestMethod(REQUEST_METHOD);
                connection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                connection.setRequestProperty("Accept", "application/json");
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setConnectTimeout(CONNECTION_TIMEOUT);
                DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
                wr.writeBytes(object.toString());
                wr.flush();
                wr.close();
                connection.getOutputStream().close();
                responseCode = connection.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    InputStream in = connection.getInputStream();
                    in.close();
                } else {
                }
            } catch (IOException e) {
                e.printStackTrace();
                result = null;
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return result;
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null && responseCode == HttpURLConnection.HTTP_OK) {
                Toast.makeText(ClientRegister.this, "Transfer data USER succesed !", Toast.LENGTH_LONG).show();
                finish();
            } else if (result == null) {
                Toast.makeText(ClientRegister.this, "Failed send USER data !", Toast.LENGTH_LONG).show();
                finish();
            } else {
                Toast.makeText(ClientRegister.this, "Update USER data!", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

    public void initUi() {
//        firstName = findViewById(R.id.label_first_name);
//        lastName = findViewById(R.id.label_last_name);
        idCard = findViewById(R.id.label_tag_id);
        camera = findViewById(R.id.label_camera);
    }

    private JSONObject createJsonObject() throws JSONException {
        String userID = getUserId();
        String id = idCard.getText().toString();
        String numberCamera = camera.getText().toString();

        JSONObject jsonObject = new JSONObject();
        jsonObject.accumulate("userId", userID);
        jsonObject.accumulate("idCard", id);
        jsonObject.accumulate("apartmentNumber", numberCamera);

        return jsonObject;
    }

    private String getUserId() {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        String userId = settings.getString("user_id", "");
        return userId;
    }
}
