package android.learning.com.nfcdoorlock.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

public class UserUtils {

    public static boolean isUserLoggedIn(Context context){
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        String userId = settings.getString("user_id", "");
        return !TextUtils.isEmpty(userId);
    }
}
